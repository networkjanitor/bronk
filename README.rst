bronk
=====

Installation
------------

For some reason pip or setuptools do not want to respect the dependency_links attribute. Maybe I am also to dumb to set
it up right ... whatever. Use a patched `urwid` version, which allows for clipped labels in buttons.

`pip install git+https://gitlab.com/networkjanitor/urwid.git`


